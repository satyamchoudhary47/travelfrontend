export interface Isubcat {
    subcat_id:number;
    cat_id:number;
    subcat_name:string;
    subcat_img:string;
    subcat_desc:string;
}
