export interface Icategory {
    cat_id:number;
    cat_name:string;
    cat_img:string;
    cat_desc:string;
    has_subcat:number;
}
