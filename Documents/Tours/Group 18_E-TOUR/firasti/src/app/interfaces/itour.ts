export interface Itour {
    tour_id:number;
    tour_name:string;
    tour_cat:number;
    tour_subcat:number;
    tour_img:string;
    tour_desc:string;
}
