export interface IbookingMaster {
    bkid:number;
    userid:number;
    bktourid:number;
    dtid:number;
    noofperson:number;
    totcost:number; 
    cstid:number;
}
