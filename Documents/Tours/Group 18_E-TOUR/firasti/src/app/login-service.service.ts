import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class LoginServiceService {
  url = 'http://localhost:3000/';

  user: any = {};

  id: any;

  sid: any;

  tid: any;

  tourDetails: any = [];

  constructor(private http: HttpClient) {}

  getUser(email: string, pass: any) {
    return this.http.get(this.url + 'api/user/' + email + '/pass/' + pass);
  }

  getCat() {
    return this.http.get(this.url + '/all-cat');
  }

  getSubcat(id) {
    return this.http.get(this.url + '/all-subcat/' + id);
  }

  getAuthUser(email: string, password: string) {
    return this.http.get(
      this.url + '/search-user-by-email-password/' + email + '&' + password
    );
  }

  getToursBySid(sid) {
    return this.http.get(this.url + '/tours-by-sid/' + sid);
  }

  getTourByUserid(userid: any) {
    return this.http.get(this.url + '/booking/' + userid);
  }

  getTourNameByTid(tid) {
    return this.http.get(this.url + '/tours/' + tid);
  }

  getTourDateByTid(tid) {
    return this.http.get(this.url + '/tour/datedetails/' + tid);
  }

  getCostByTid(tid) {
    return this.http.get(this.url + '/tour/costdetail/' + tid);
  }
}
