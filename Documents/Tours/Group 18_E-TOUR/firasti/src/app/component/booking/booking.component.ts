import { Component, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorageService } from 'angular-web-storage';
import { BookingMaster } from 'src/app/entities/booking-master';
import { IbookingMaster } from 'src/app/interfaces/ibooking-master';
import { Icost } from 'src/app/interfaces/icost';
import { Idate } from 'src/app/interfaces/idate';
import { Iiternary } from 'src/app/interfaces/iiternary';
import { Itour } from 'src/app/interfaces/itour';
import { LoginServiceService } from 'src/app/login-service.service';
import { BookingService } from 'src/app/services/booking.service';
import { TourService } from 'src/app/services/tour.service';
declare var Razorpay: any;

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit {
 
  tourid:number;
  userid:number;
  tour:Itour;
  tourdates:Idate[];
  tourcosts:Icost[];
  touritr: Iiternary[];
  cost0:number;
  cost1:number;
  cost2:number;
  cost3:number;
  cost4:number;
  ip0:number;
  ip1:number;
  ip2:number;
  ip3:number;
  ip4:number;
  bookingrecord:IbookingMaster;
  bookForm: FormGroup;
  submitted = false;
  totalcost = 0;
  errormsg:string="";
  constructor(public fb: FormBuilder,private _tourserv:TourService,
    private _activatedRoute: ActivatedRoute,
    private router:Router,private _bookserv:BookingService,private storage:LocalStorageService,private service:LoginServiceService) {  }

  ngOnInit(): void {
      let id1: string = this._activatedRoute.snapshot.params['tourid'];
      let id:number= parseInt(id1);
      this.tourid=id;
      var users = this.storage.get('user');
      this.userid = users.id;
      this.getTour(this.tourid);
      this.getDateOptions(this.tourid);
      this.getItrOptions(this.tourid);  
      this.getCostOptions(this.tourid);
      this.buildBookForm();
  }

  
  buildBookForm() 
  {
    this.bookForm = this.fb.group
    ({
      bk_id:[0],
      user_id:[this.userid],
      bk_tour_id:[this.tourid],
      dt_id: ['',[Validators.required]],
      no_of_person: ['',[Validators.required]],
      tot_cost:[this.totalcost,[Validators.required]],
      cst_id:[1],
      cost0:[0],
      cost1:[0],
      cost2:[0],
      cost3:[0],
      cost4:[0],
      ip0:[0],
      ip1:[0],
      ip2:[0],
      ip3:[0],
      ip4:[0],
      no_of_adults: [''],
      no_of_children: ['']

    });
    this.onValueChanges();
  }

  onValueChanges(): void {
    this.bookForm.valueChanges.subscribe(val=>{

      let tot0,tot1,tot2,tot3,tot4;

      if(val.cost0)
      {
        tot0=7000;
        tot0=tot0*val.ip0;
      }
      else{
        tot0=0;
        this.bookForm.get("ip0").setValue(0, { onlySelf: true });
      }
      if(val.cost1)
      {
        tot1=4000;
        tot1=tot1*val.ip1;
      }
      else{
        tot1=0;
        this.bookForm.get("ip1").setValue(0, { onlySelf: true });
      }
      if(val.cost2)
      {
        tot2=2500;
        tot2=tot2*val.ip2;
      }
      else{
        tot2=0;
        this.bookForm.get("ip2").setValue(0, { onlySelf: true });
      }
      if(val.cost3)
      {
        tot3=2000;
        tot3=tot3*val.ip3;
      }
      else{
        tot3=0;
        this.bookForm.get("ip3").setValue(0, { onlySelf: true });
      }
      if(val.cost4)
      {
        tot4=1000;
        tot4=tot4*val.ip4;
      }
      else{
        tot4=0;
        this.bookForm.get("ip4").setValue(0, { onlySelf: true });
      }
      let persons = val.no_of_person;
      let adults = val.no_of_adults;
      let children = val.no_of_children;
      if(persons != adults+children)
        this.errormsg="Please enter correct values.";
      else
        this.errormsg="";
      let total: number;
      total = tot0+tot1+tot2+tot3+tot4;
      //console.log(total);
      this.setvaleofcost(total);
    })
  }

  setvaleofcost(selectedValue)
  {
    this.totalcost = selectedValue;
    this.bookForm.get("tot_cost").setValue(selectedValue, { onlySelf: true });
  }

  get no_of_person() {
    return this.bookForm.get('no_of_person');
  }

  get tot_cost() {
    return this.bookForm.get('tot_cost');
  }

  get cst_id() {
    return this.bookForm.get('cst_id');
  }

  get dt_id() {
    return this.bookForm.get('dt_id');
  }

  get no_of_adults() {
    return this.bookForm.get('no_of_adults');
  }

  get no_of_children() {
    return this.bookForm.get('no_of_children');
  }

  onSubmit(bookForm: FormGroup) {
    this.submitted = true;
    if (!bookForm.valid) {
      console.log(bookForm.value);
      return;
    }
    this.mapFormValues(bookForm); 
  }

  mapFormValues(form: FormGroup) {
    this.bookingrecord = new BookingMaster (0,0,0,0,0,0,0);
    this.bookingrecord.bkid = 0;
    this.bookingrecord.userid = this.userid;
    this.bookingrecord.bktourid = this.tourid;
    this.bookingrecord.cstid = 1;
    let getdtid = form.controls.dt_id.value;
    this.bookingrecord.dtid = getdtid.dtid;
    //let getcstid = form.controls.cst_id.value;
    //this.bookingrecord.cstid= getcstid.cstid;
    this.bookingrecord.noofperson = form.controls.no_of_person.value;
    this.bookingrecord.totcost = form.controls.tot_cost.value;
    this.postdata(this.bookingrecord);
  }

  postdata(bookingobj:IbookingMaster) {
   // console.log(bookingobj);
    this._bookserv.bookTour(bookingobj).subscribe();
    this.payment();
  }

  getTour(id)
  {
    this._tourserv.getTour(id).subscribe( data=>{this.tour=data} );
  } 

  getItrOptions(id)
  {
    this._tourserv.getItrOptions(id).subscribe(data=>{ this.touritr=data; } );
  } 

  getDateOptions(id)
  {
    this._tourserv.getDateOptions(id).subscribe(data=>{ this.tourdates=data; } );
  }

  getCostOptions(id)
  {
    this._tourserv.getCostOptions(id).subscribe(data=>{ this.tourcosts=data; } );
  }


  //Payment

  payment() {
    //console.log("do payment of "+this.totalcost )

    var options = {
      "key": "rzp_test_V2nxpss5nnkCRr", // Enter the Key ID generated from the Dashboard
      "amount": this.totalcost*100, // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
      "currency": "INR",
      "name": "Firasti ",
      "description": this.tour.tour_name,
      "image": "https://cdn.investintech.com/wp-content/uploads/2018/10/converting-jpeg-to-pdf.png",
      "handler": function (response) {
      },
      "prefill": {
        "name": this.service.user.name,
        "email": this.service.user.email,
        "contact": this.service.user.mobile
      },
      "notes": {
        "address": "Razorpay Corporate Office"
      },
      "theme": {
        "color": "#3399cc"
      }
    };  
    var rzp1 = new Razorpay(options);
    rzp1.on('payment.failed', function (response) {
    });
    var rzp1 = new Razorpay(options);

    rzp1.open();

  }
}
