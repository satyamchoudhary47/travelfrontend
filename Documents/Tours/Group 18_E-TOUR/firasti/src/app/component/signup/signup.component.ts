import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SignupServiceService } from 'src/app/signup-service.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent implements OnInit {
  userlist: any;
  success: any;
  uData: any = {};

  constructor(
    private signupService: SignupServiceService,

    public router: Router
  ) {
    this.userlist = [];
  }
  clearFields() {
    this.uData = {};
    this.router.navigateByUrl('/signup');
  }

  showUsers() {
    this.signupService.getUsers().subscribe((res) => (this.userlist = res));
  }

  ngOnInit(): void {
    // this.showUsers();
  }

  addUser() {
    var data = {
      name: this.uData.name,
      email: this.uData.email,
      password: this.uData.password,
      mobile: this.uData.mobile,
    };
    //console.log("data to create ",data)
    this.success = this.signupService.addUser(data).subscribe((res: any) => {
      // console.log("****",res);
      if (res.code == 200) {
        alert(res.message);
        this.router.navigateByUrl('/login');
      } else if (res.keyValue.email) {
        alert('Email Id Exits');
      }
    });
  }
}
