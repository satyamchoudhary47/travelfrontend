import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorageService } from 'angular-web-storage';
import { Icost } from 'src/app/interfaces/icost';
import { Idate } from 'src/app/interfaces/idate';
import { Iiternary } from 'src/app/interfaces/iiternary';
import { Itour } from 'src/app/interfaces/itour';
import { TourService } from 'src/app/services/tour.service';

@Component({
  selector: 'app-tour-detail',
  templateUrl: './tour-detail.component.html',
  styleUrls: ['./tour-detail.component.css']
})
export class TourDetailComponent implements OnInit {
 
  complete:boolean=false;
  tourid:number;
  tour:Itour;
  tourdates:Idate[];
  tourcosts:Icost[];
  touritr: Iiternary[];

  constructor(private _tourserv:TourService,
    private _activatedRoute: ActivatedRoute,
    private router:Router,public storage:LocalStorageService) { }

  ngOnInit(): void 
  {
    let id1: string = this._activatedRoute.snapshot.params['id'];
    let id:number= parseInt(id1);
    this.tourid=id;
    this.getTour(this.tourid);
    this.getDateOptions(id);
    this.getItrOptions(id);  
    this.getCostOptions(id);
   }

  getTourByCat(catid)
  {
    this._tourserv.getTourByCat(catid).subscribe( data=>{this.tourid=data.tour_id;this.complete=true;} );
  }
 
  getTour(tourid)
  {
    this._tourserv.getTour(tourid).subscribe( data=>{this.tour=data} );
  } 

  getItrOptions(id)
  {
    this._tourserv.getItrOptions(id).subscribe(data=>{ this.touritr=data; } );
  } 

  getDateOptions(id)
  {
    this._tourserv.getDateOptions(id).subscribe(data=>{ this.tourdates=data; } );
  }

  getCostOptions(id)
  {
    this._tourserv.getCostOptions(id).subscribe(data=>{ this.tourcosts=data; } );
  }

  onbook()
  {
    //console.log("in method"+this.tourid);
    //this.router.navigateByUrl(`/booking/${this.tourid}`);
    var users = this.storage.get('user')
    if(users){
      this.router.navigateByUrl(`/booking/${this.tourid}`);
    }
    else{
      this.router.navigateByUrl('/login')
    }
  }
}
