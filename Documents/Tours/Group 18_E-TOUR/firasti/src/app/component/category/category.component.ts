import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorageService } from 'angular-web-storage';
import { Icategory } from 'src/app/interfaces/icategory';
import { CategoryService } from 'src/app/services/category.service';
import { TourService } from 'src/app/services/tour.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  tourid:number;
  tour:any;
  categories:Icategory[];
  constructor(private _catserv:CategoryService,
            private _activatedRoute: ActivatedRoute,
            private router:Router,
            private _tourserv:TourService,private storage:LocalStorageService) { }

  ngOnInit(): void {
    this._activatedRoute.params.subscribe((params)=>{
      if (params['all']) { this.getall(); }
      else if (params['id']) { this.getsubcat(); }
      else if(params['gettourbycat']){this.gettourbycat()}
    });
  }

  getall()
  {
    this._catserv.getCategories().subscribe(data=>this.categories=data);
  } 

  getsubcat()
  {
    let id: string = this._activatedRoute.snapshot.params['id'];
    let catid:number= parseInt(id);
    this.router.navigateByUrl(`/subcat/${id}`);
  }

  gettourbycat()
  {
    let id: string = this._activatedRoute.snapshot.params['gettourbycat'];
    let catid:number= parseInt(id);
    this._tourserv.getTourByCat(catid).subscribe( data=>{this.tour=data;this._tourserv.tourid=this.tour.tourid;console.log(this._tourserv.tourid);this.router.navigateByUrl(`/tourdetails/${this._tourserv.tourid}`);} )
   
    // this._catserv.getCategory(catid).subscribe(  )
  }

 
}
