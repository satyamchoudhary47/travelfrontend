import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'angular-web-storage';
import { LoginServiceService } from 'src/app/login-service.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  user:any;

  hidden = false;
  
  constructor(private storage: LocalStorageService,
    public loginService:LoginServiceService,
    public route:Router) { }

  

  toggleBadgeVisibility() {
    this.hidden = !this.hidden;
  }

  ngOnInit(): void {
   
  }

  ngAfterContentInit(){
    //console.log("ngAfterContentInit")
    this.loginService.user = this.storage.get('user');
    //console.log(this.loginService.user)

  }

  logout()
  {
    this.storage.clear();
    this.route.navigateByUrl("/home");
    alert("Logged out! Bye-Bye!")
    this.ngAfterContentInit()
  }

  goToHome(){
    this.route.navigateByUrl("/home");
    
  }
}
