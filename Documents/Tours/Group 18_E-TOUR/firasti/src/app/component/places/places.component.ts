import { Component, OnInit } from '@angular/core';
import { Itour } from 'src/app/interfaces/itour';
import { TourService } from 'src/app/services/tour.service';

@Component({
  selector: 'app-places',
  templateUrl: './places.component.html',
  styleUrls: ['./places.component.css']
})
export class PlacesComponent implements OnInit {

  constructor(private _tourserv:TourService) { }

  tour1:Itour;
  tour2:Itour;
  tour3:Itour;
  ngOnInit(): void {
    this._tourserv.getTour(1).subscribe( data=>this.tour1=data );
    this._tourserv.getTour(2).subscribe( data=>this.tour2=data );
    this._tourserv.getTour(3).subscribe( data=>this.tour3=data );
  }

}
