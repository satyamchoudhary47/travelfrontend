import { Component, OnInit } from '@angular/core';
import { CategoryService } from 'src/app/services/category.service';

@Component({
  selector: 'app-category-detail',
  templateUrl: './category-detail.component.html',
  styleUrls: ['./category-detail.component.css']
})
export class CategoryDetailComponent implements OnInit {
  private _activatedRoute: any;
  
  constructor(private _catserv:CategoryService) { }
  catid:number;
  category:any;
  
  ngOnInit(): void {
    let id1: string = this._activatedRoute.snapshot.params['catid'];
    let id:number= parseInt(id1);
    this.catid=id;
    this.getDetail(this.catid);
  }

  getDetail(catid)
  {
    this._catserv.getCategory(catid).subscribe(data=>this.category=data);
  } 
}
