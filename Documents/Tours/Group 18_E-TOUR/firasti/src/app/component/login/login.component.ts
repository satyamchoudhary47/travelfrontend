import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'angular-web-storage';
import { User } from 'src/app/user';
import { LoginServiceService } from '../../login-service.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginData: any = {};
  seachData: any = {};
  
  previousUrl: string;

  constructor(public route: Router,
    private storage: LocalStorageService,
    private loginService: LoginServiceService) { }

  ngOnInit(): void {
   }

  ngAfterContentInit(){
    console.log(this.storage.get('user'))

  }

  go() {
    this.route.navigate(['app-signup']);
  }

  checkAuth() {
    var email = this.loginData.email;
    var password = this.loginData.password;
    //console.log(email + password);

    this.loginService.getUser(email,password).subscribe(res => {
      if (res) {
        this.loginService.user=res
        this.storage.set("user", res)
        //console.log(res);
        this.route.navigateByUrl('/home')
        alert("Logged in successfully. ");
        //navigate to homepage
      } else {
        alert("Please enter valid email id")
      }
    })
  }

  checkUserAuth() {
    var email = this.loginData.email;
    var password = this.loginData.password;
    //console.log(email + password);

    this.loginService.getUser(email,password).subscribe(res => {
      if (res) {
        this.loginService.user=res
        this.storage.set("user", res)
        //console.log(res);
        this.route.navigateByUrl('/home')
        alert("Logged in successfully. ");
        //navigate to homepage
      } else {
        alert("Please enter valid email id & password")
      }
    });
  }
}
