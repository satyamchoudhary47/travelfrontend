import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Isubcat } from 'src/app/interfaces/isubcat';
import { CategoryService } from 'src/app/services/category.service';

@Component({
  selector: 'app-sucategory',
  templateUrl: './sucategory.component.html',
  styleUrls: ['./sucategory.component.css']
})
export class SucategoryComponent implements OnInit {

  subcategories:Isubcat[];

  constructor(private _catserv:CategoryService,
    private _activatedRoute: ActivatedRoute,
    private router:Router) { }

  ngOnInit(): void {
    let id: string = this._activatedRoute.snapshot.params['id'];
    let catid:number= parseInt(id);
    console.log("catid",catid)
    this.getall(catid);
  }

  getall(catid)
  {
    this._catserv.getSubcategories(catid).subscribe(  data=>this.subcategories=data );
  } 

}
