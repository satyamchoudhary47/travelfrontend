import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategoryService } from 'src/app/services/category.service';
import { TourService } from 'src/app/services/tour.service';

@Component({
  selector: 'app-home-cat',
  templateUrl: './home-cat.component.html',
  styleUrls: ['./home-cat.component.css'],
})
export class HomeCatComponent implements OnInit {
  categories: any[];
  constructor(
    private _catserv: CategoryService,
    public router: Router,
    private _tourserv: TourService
  ) {}
  ngOnInit(): void {
    // this._catserv.getCategories().subscribe((data) => (this.categories = data));
    this._catserv.getCategories().subscribe(res =>{
      console.log("categories res",res)
      this.categories = res
    })
  }
}
