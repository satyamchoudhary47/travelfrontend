import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'angular-web-storage';
import { LoginServiceService } from 'src/app/login-service.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})


export class ProfileComponent implements OnInit {

  user: any = [];
  userid: any;
  tourDetails: any = [];
  showHistory: boolean = false;
  tourid: any
  totcost: any
  dtid: any

  //  profile {tourname, start date, end date, cost};
  history = [];
  profile = {
    "tourname": "",
    "startdate": "",
    "enddate": "",
    "cost": ""
  }

  constructor(private service: LoginServiceService,private router:Router,private storage:LocalStorageService) { }

  ngOnInit(): void {
    var users = this.storage.get('user')
    if(users){
      this.user = this.service.user;
      this.userid = this.service.user.id;
      this.getTourDetailsById();
    }
    else{
      this.router.navigateByUrl('/login')
    }
  }

  async getTourDetailsById() {
    let data = await this.service.getTourByUserid(this.userid).toPromise();
    this.tourDetails = data
    for (let tour of this.tourDetails) {
      this.tourid = tour.bktourid
      this.dtid = tour.dtid
      this.totcost = tour.totcost
      let tourname: any = await this.service.getTourNameByTid(this.tourid).toPromise();
      let tourdate: any = await this.service.getTourDateByTid(this.dtid).toPromise();
      this.profile = {
        "tourname": tourname.tourname ,
        "startdate":  tourdate.startdt,
        "enddate": tourdate.enddt,
        "cost": this.totcost
      }
      this.history.push(this.profile)
    }
  }
  
  getMyHistory() {
    this.showHistory = true;
  }
}
