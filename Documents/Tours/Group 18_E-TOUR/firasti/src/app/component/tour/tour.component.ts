import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Itour } from 'src/app/interfaces/itour';
import { TourService } from 'src/app/services/tour.service';

@Component({
  selector: 'app-tour',
  templateUrl: './tour.component.html',
  styleUrls: ['./tour.component.css']
})
export class TourComponent implements OnInit {

  tours:Itour[];
  
  constructor(private _tourserv:TourService,
    private _activatedRoute: ActivatedRoute,
    private router:Router) { }

  ngOnInit(): void 
  {
      let id1: string = this._activatedRoute.snapshot.params['id1'];
      let catid:number= parseInt(id1);
      let id2: string = this._activatedRoute.snapshot.params['id2'];
      let subcatid:number= parseInt(id2);
      this.getall(catid,subcatid);
  }

  getall(catid,subcatid)
  {
    this._tourserv.getTours(catid,subcatid).subscribe( data=>this.tours=data );
  } 

}
