import { Iiternary } from "../interfaces/iiternary";

export class Iternary implements Iiternary {
    public constructor(public itr_id:number,
        public it_tour_id:number,
        public day_no:number,
        public desc:string){}
}
