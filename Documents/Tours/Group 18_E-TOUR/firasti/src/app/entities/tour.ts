import { Itour } from "../interfaces/itour";

export class Tour implements Itour{
    public constructor(    
        public tour_id:number,
        public tour_name:string,
        public tour_cat:number,
        public tour_subcat:number,
        public tour_img:string,
        public tour_desc:string){}
}
