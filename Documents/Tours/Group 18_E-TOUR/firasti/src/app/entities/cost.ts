import { Icost } from "../interfaces/icost";

export class Cost implements Icost {
    constructor(public cst_id:number,
        public cst_tour_id:number,
        public cst_type:string,
        public cost:number){}
}
