import { Idate } from "../interfaces/idate";

export class Date implements Idate{
    public constructor(public dt_id:number,
        public tour_id:number,
        public start_dt:number,
        public end_dt:number,
        public no_of_days:number){}
}
