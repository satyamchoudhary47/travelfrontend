import { IbookingSupport } from "../interfaces/ibooking-support";

export class BookingSupport implements IbookingSupport{
   public constructor(public bk_detail_id:number,
    public bk_id:number,
    public pname:string,
    public dt_id:number,
    public age:number){} 
}
