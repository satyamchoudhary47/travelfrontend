import { IbookingMaster } from "../interfaces/ibooking-master";

export class BookingMaster implements IbookingMaster{
    public constructor(
        public bkid:number,
        public userid:number,
        public bktourid:number,
        public dtid:number,
        public noofperson:number,
        public totcost:number,
        public cstid:number
    ){}
}
