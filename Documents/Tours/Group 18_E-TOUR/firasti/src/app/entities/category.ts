import { Icategory } from "../interfaces/icategory";

export class Category implements Icategory{
    public constructor( 
        public cat_id:number,
        public cat_name:string,
        public cat_img:string,
        public cat_desc:string,
        public has_subcat:number)
        {}
}
