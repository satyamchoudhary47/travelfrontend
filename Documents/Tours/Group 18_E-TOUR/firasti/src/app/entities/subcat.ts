import { Isubcat } from "../interfaces/isubcat";

export class Subcat implements Isubcat {
    public constructor(
        public subcat_id:number,
        public cat_id:number,
        public subcat_name:string,
        public subcat_img:string,
        public subcat_desc:string,)
        {}
}
