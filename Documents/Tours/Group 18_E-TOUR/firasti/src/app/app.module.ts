import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './component/home/home.component';
import { PlacesComponent } from './component/places/places.component';
import { ContactusComponent } from './component/contactus/contactus.component';
import { ServicesComponent } from './component/services/services.component';
import { FooterComponent } from './component/footer/footer.component';
import { LoginComponent } from './component/login/login.component';
import { SignupComponent } from './component/signup/signup.component';
import { NavigationComponent } from './component/navigation/navigation.component';
import { LoginServiceService } from './login-service.service';
import { SignupServiceService } from './signup-service.service';
import { CategoryComponent } from './component/category/category.component';
import { CategoryDetailComponent } from './component/category-detail/category-detail.component';
import { PageNotFoundComponentComponent } from './component/page-not-found-component/page-not-found-component.component';
import { SucategoryComponent } from './component/sucategory/sucategory.component';
import { TourComponent } from './component/tour/tour.component';
import { TourDetailComponent } from './component/tour-detail/tour-detail.component';
import { BookingComponent } from './component/booking/booking.component';
import { HomeCatComponent } from './component/home-cat/home-cat.component';
import { ProfileComponent } from './component/profile/profile.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PlacesComponent,
    ContactusComponent,
    ServicesComponent,
    FooterComponent,
    LoginComponent,
    SignupComponent,
    NavigationComponent,
    CategoryComponent,
    CategoryDetailComponent,
    PageNotFoundComponentComponent,
    SucategoryComponent,
    TourComponent,
    TourDetailComponent,
    BookingComponent,
    HomeCatComponent,
    ProfileComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [LoginServiceService,SignupServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
