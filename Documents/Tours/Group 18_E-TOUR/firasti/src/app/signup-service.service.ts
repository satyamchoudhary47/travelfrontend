import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Iuser } from './iuser';


@Injectable({
  providedIn: 'root'
})
export class SignupServiceService {

  url="http://localhost:3000/api";

  constructor( private http : HttpClient) { }

  getUsers(): Observable<any[]> {
    return this.http.get<any[]>(this.url + '/all-users');
  }

  addUser(data: any) {
    return this.http.post(this.url + '/register' , data);
  }

}
