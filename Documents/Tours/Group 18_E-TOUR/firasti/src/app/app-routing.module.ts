import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { BookingComponent } from './component/booking/booking.component';
import { CategoryDetailComponent } from './component/category-detail/category-detail.component';
import { CategoryComponent } from './component/category/category.component';
import { ContactusComponent } from './component/contactus/contactus.component';
import { HomeComponent } from './component/home/home.component';
import { LoginComponent } from './component/login/login.component';
import { PageNotFoundComponentComponent } from './component/page-not-found-component/page-not-found-component.component';
import { PlacesComponent } from './component/places/places.component';
import { ProfileComponent } from './component/profile/profile.component';
import { ServicesComponent } from './component/services/services.component';
import { SignupComponent } from './component/signup/signup.component';
import { SucategoryComponent } from './component/sucategory/sucategory.component';
import { TourDetailComponent } from './component/tour-detail/tour-detail.component';
import { TourComponent } from './component/tour/tour.component';

const routes: Routes = [
  { path: 'signup', component: SignupComponent },
  { path: 'home', component: HomeComponent },
  { path: 'places', component: PlacesComponent },
  { path: 'services', component: ServicesComponent },
  { path: 'contactus', component: ContactusComponent },
  { path: 'login', component: LoginComponent },
  { path: 'gettourbycat/:gettourbycat', component: CategoryComponent },
  { path: 'category/:all', component: CategoryComponent },
  { path: 'catdetail/:id', component: CategoryDetailComponent},
  { path: 'subcat/:id', component: SucategoryComponent},
  { path: 'tour/:id1/:id2', component: TourComponent},
  { path: 'tourdetails/:id', component: TourDetailComponent},
  { path: 'booking/:tourid', component: BookingComponent},
  { path: 'profile', component: ProfileComponent},
  {
    path: "",
    redirectTo: '/home',
    pathMatch: 'full'
  },
  { path: '**', component: PageNotFoundComponentComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
