import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Tour } from '../entities/tour';
import { Icost } from '../interfaces/icost';
import { Idate } from '../interfaces/idate';
import { Iiternary } from '../interfaces/iiternary';
import { Itour } from '../interfaces/itour';

@Injectable({
  providedIn: 'root'
})
export class TourService {
  url="http://localhost:8080";

  constructor( private http : HttpClient) { }
  tourid:number;
  getTour(tourid:number):Observable<Tour>{
    return this.http.get<Tour>(this.url + '/tours/'+tourid)
  }

  getTours(catid:number,subcatid:number):Observable<Itour[]>{
    return this.http.get<Itour[]>(this.url + '/tours/' + catid+'/'+subcatid)
  }

  getTourByCat(catid:number):Observable<Tour>{
    return this.http.get<Tour>(this.url + '/tours/gettourbycat/'+catid)
  }

  getItrOptions(id:number):Observable<Iiternary[]>{
    return this.http.get<Iiternary[]>(this.url + '/tour/itroptions/'+id)
  }

  getItrDetails(id:number):Observable<Iiternary>{
    return this.http.get<Iiternary>(this.url + '/tour/itrdetails/'+id)
  }

  getDateOptions(id:number):Observable<Idate[]>
  {
    return this.http.get<Idate[]>(this.url + '/tour/dateoptions/'+id)
  }

  getDate(id:number):Observable<Idate>
  {
    return this.http.get<Idate>(this.url + '/tour/datedetails/'+id)
  }

  getCostOptions(id:number):Observable<Icost[]>
  {
    return this.http.get<Icost[]>(this.url + '/tour/costoptions/'+id)
  }

  getCost(id:number):Observable<Icost>
  {
    return this.http.get<Icost>(this.url + '/tour/costdetail/'+id)
  }

}