import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IbookingMaster } from '../interfaces/ibooking-master';

@Injectable({
  providedIn: 'root'
})
export class BookingService {
  url="http://localhost:8080";

  constructor( private http : HttpClient) { }

  bookTour(bookingobj:IbookingMaster)
  {
    console.log(bookingobj);
    // console.log(bookingobj.bk_id + ": " +typeof(bookingobj.bk_id));
    // console.log(bookingobj.user_id + ": "+typeof(bookingobj.user_id));
    return this.http.post<IbookingMaster>(this.url+"/booking/addbooking/",bookingobj);
  }

}
