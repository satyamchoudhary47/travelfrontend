import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Category } from '../entities/category';
import { Icategory } from '../interfaces/icategory';
import { Isubcat } from '../interfaces/isubcat';

@Injectable({
  providedIn: 'root'
})

export class CategoryService {
  url="http://localhost:3000/";

  constructor( private http : HttpClient) { }

  getCategories(): Observable<any> {
    return this.http.get<any>(this.url + 'api/categories')
  }

  getCategory(id:number):Observable<any>{
    return this.http.get<any>(this.url + '/category/getdetails/' + id)
  }

  getSubcategories(id:number):Observable<any>{
    return this.http.get<any>(this.url + 'api/packages/' + id)
  }
  
}
